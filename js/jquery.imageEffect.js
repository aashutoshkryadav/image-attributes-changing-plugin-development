(function ($){
$.fn.imageEffect = function(options) {
				var defaults = {  
					border : '1px solid #cccccc',
					background : '#f7f7f7',
					margin : 10,
					padding : 10,
					opacity:0.1
               		};
                 
            
var settings =  $.extend(defaults, options); 

return this.each(function() {  
			$(this).css({
					border : settings.border,
					background: settings.background,
					margin : settings.margin,
					padding : settings.padding,
					opacity : settings.opacity
						});
    				});
};
})(jQuery);
